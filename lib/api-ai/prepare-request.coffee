apiai = require('apiai')

module.exports = (token, reply) ->
  question = decodeURIComponent reply
  apiAiInstance = apiai("#{token}")
  request = apiAiInstance.textRequest(question, sessionId: '123456')
