DatabaseConnector = require('../config/DatabaseConnector')
TimetableResultHandling = require('../lib/TimetableResultHandling')
TimetableCondition = require('./TimetableCondition')
Promise = require 'bluebird'
log = require '../lib/utils/log'


module.exports = class QueryService

  constructor: ->
    @knexConnection = new DatabaseConnector().getConnection()

  executeTimetableQuery: (apiAitoken, apiAiPayload) ->
    try
      queryResponseData = await @_buildTimetableCondition apiAitoken, apiAiPayload
      result = await new TimetableResultHandling(queryResponseData).getResults()
      return result
    catch error
      throw err

  _buildTimetableCondition: (apiAitoken, apiAiPayload) ->
    query = new TimetableCondition(apiAiPayload, @knexConnection)
    .get()
    .andWhere(() -> @where('token', apiAitoken))
    
