config = require '../config/config'

module.exports = class StageNavigationService

  constructor: (apiAiPayload) ->
    @_mapStageNavigationPayloadToLocalProperties(apiAiPayload)
    @getCard()

  getCard: () ->

    if @stage is "Zeppelin Stage"
      card = "cards": [
        "cardTitle": "Zeppelin Stage",
        "cardSubtitle": "Die Stage ist im hinteren Teil des Festivalgeländes. Vom Eingang kommend, an Gate D
        rechts abbiegen. An Food- und Merchandiseständen vorbei links halten. Dann der Beschilderung folgen.",
        "cardImage":"https://media05.onetz.de/2017/06/02/713146_web.jpg?1496436489",
        "cardLink":"https://assets.mlk-festivals.com/attachment/1375/file/2073ce37577c4d8b4a713a0358ff729c.jpg"]

    else
      card = "cards": [
        "cardTitle": "Becks Park Stage",
        "cardSubtitle": "Vom Eingang kommen, an Gate D rechts abbiegen. Anschließend rechts halten bis zum Eingangsbereich der Stage.",
        "cardImage":"https://goo.gl/xTqozk",
        "cardLink":"https://assets.mlk-festivals.com/attachment/1375/file/2073ce37577c4d8b4a713a0358ff729c.jpg"]

    return card


  _mapStageNavigationPayloadToLocalProperties: (apiAiPayload) ->
    for localProperty, apiAiProperty of config.mappings.stageNavigation
      @[localProperty] = apiAiPayload.parameters[apiAiProperty]
