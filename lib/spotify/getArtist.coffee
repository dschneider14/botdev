request = require 'request'
Promise = require 'bluebird'
getToken = require './getToken'
util = require 'util'

module.exports =
  (artist) ->
    token = await util.promisify(getToken)()
    new Promise (resolve, reject) ->
      request
        method: 'GET',
        uri: 'https://api.spotify.com/v1/search',
        headers:
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: token
        qs:
          q: artist,
          type: "artist",
          market: "DE",
          limit: "1"
      ,
        (error, response, body) ->
          if error
            reject error
          else
            body = JSON.parse body
            link = if body.artists?.items[0]?.external_urls.spotify is undefined then " " else body.artists?.items[0]?.external_urls.spotify
            image = if body.artists?.items[0]?.images[1]?.url is undefined then "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/ITunes_12.2_logo.png/600px-ITunes_12.2_logo.png" else body.artists?.items[0]?.images[1]?.url
            resolve artist =
                      link: link
                      image: image
