request = require 'request'
Promise = require 'bluebird'

module.exports = (done) ->

    requestOptions =
      method: 'POST',
      uri: 'https://accounts.spotify.com/api/token',
      headers:
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: "Basic M2JmNDljY2RkNWZmNDAzYmE2YmE1Y2JjZmQyMDk2OWU6Y2JkZGNmOGZhMGY1NGRjN2IzMWUzMjIzMzRlMzQ1OWU="
      qs:
        grant_type: "client_credentials"
    request requestOptions, (error, response, body) ->
      if error
        return done error
        
      else
        body = JSON.parse body
        access_token = body.token_type + " " + body.access_token
        return done null, access_token
