config = require '../../config/config'
_ = require 'lodash'

module.exports = class NavigationService

  constructor: (apiAiPayload) ->
    @_mapApiAiNavigationPayloadToLocalProperties(apiAiPayload)
    @getNavigation(apiAiPayload)

  getNavigation: (apiAiPayload) ->

    modifiedLocation = @stadt + " " + @strasse + " " + @postleitzahl

    if @startLocation isnt ''
      modifiedLocation = "Current+Location"

    navigationCard = "cards": [
                      "cardTitle": "Anreise",
                      "cardSubtitle": "Hier ist dein Weg zum Festival 🗺 ↩️",
                      "cardImage": "https://www.seeklogo.net/wp-content/uploads/2015/09/new-google-maps-logo-vector-download.jpg",
                      "cardLink": "https://www.google.com/maps/dir/ + #{modifiedLocation} + #{config.url.festivalLocation}"]

    return navigationCard


  _mapApiAiNavigationPayloadToLocalProperties: (apiAiPayload) ->
      for localProperty, apiAiProperty of config.mappings.navigation
        @[localProperty] = apiAiPayload.parameters[apiAiProperty]
