log = require '../utils/log'

module.exports = (error) ->

  if error.syscall isnt 'listen'
    throw error

  bind = if typeof @port is 'string' then 'Pipe ' + @port else 'Port ' + @port

  #handle specific listen errors with friendly messages
  switch error.code
    when 'EACCES'
      log.error(bind + ' requires elevated privileges')
      process.exit(1)

    when 'EADDRINUSE'
      log.error(bind + ' is already in use')
      process.exit(1)

    else return error
