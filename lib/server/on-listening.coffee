log = require '../utils/log'

module.exports = () ->
  addr = this.address()
  bind = if typeof addr is 'string' then 'pipe ' + addr else 'port ' + addr.port
  log.debug('Listening on ' + bind)
