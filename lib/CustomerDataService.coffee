config = require '../config/config'
request = require 'request'
knex = require 'knex'

module.exports = class CustomerDataService

  @_instance = null

  class SingletonClass
    customerData: {}
    constructor: () ->
      console.log "Konstruktor"

    addCustomerData: (apiAiPayload, session) ->
      console.log "Add Customer Data"
      @_mapApiAiCustomerPayloadToLocalProperties(apiAiPayload, session)
      console.log "*****************************"
      console.log "Customer Data", @customerData
      if @checkData(session) then _completeRequest(session)


    updateCustomerData: (apiAiPayload, session) ->
      console.log "Update Customer Data"
      @_mapApiAiCustomerPayloadToLocalProperties(apiAiPayload, session)
      if @checkData(session, true) then _completeRequest(session)
      console.log "Customer Data", @customerData

    checkData: (session, isUpdate = false) ->
      console.log "Check Data"

      if @customerData[session].ticketanzahl?.match(/[0-9]{1}/) or (isUpdate and (@customerData[session].ticketanzahl is undefined) )
        console.log "Ticketanzahl okay!"
      else
        _correctionRequest('Ticketanzahl', session)
        return false

      if @customerData[session].geburtsdatum?.match(/^(19[789]\d|20[0123]\d)\-(0\d|1[012]|\d)\-(31|30|[012]\d|\d)$/) or (isUpdate and (@customerData[session].geburtsdatum is undefined) )
        console.log "Datum okay!"
      else
        _correctionRequest('Datum', session)
        return false

      if @customerData[session].vorname?.match(/^[A-ZäöüÄÖÜ]'?[- a-zA-ZäöüÄÖÜ]+$/) or (isUpdate and (@customerData[session].vorname is undefined) )
        console.log "Vorname okay!"
      else
        _correctionRequest('Vorname', session)
        return false

      if @customerData[session].nachname?.match(/^[A-ZäöüÄÖÜ]'?[- a-zA-ZäöüÄÖÜ]+$/) or (isUpdate and (@customerData[session].nachname is undefined) )
        console.log "Nachname okay!"
      else
        _correctionRequest('Nachname', session)
        return false

      if @customerData[session].strasse?.match(/[a-zA-ZäöüÄÖÜ\.]'?[- a-zA-ZäöüÄÖÜ]+ [0-9]+[a-zA-Z]?/) or (isUpdate and (@customerData[session].strasse is undefined) )
        console.log "Strasse und Hausnummer okay!"
      else
        _correctionRequest('Strasse', session)
        return false

      if @customerData[session].stadt?.match(/^[A-ZäöüÄÖÜ]'?[- a-zA-ZäöüÄÖÜ]+$/) or (isUpdate and (@customerData[session].stadt is undefined) )
        console.log "Stadt okay!"
      else
        _correctionRequest('Stadt', session)
        return false

      if @customerData[session].postleitzahl?.match(/[0-9]{5}/) or (isUpdate and (@customerData[session].postleitzahl is undefined) )
        console.log "Postleitzahl okay!"
      else
        _correctionRequest('Postleitzahl', session)
        return false

      if @customerData[session].email?.match(/[a-zA-Z0-9\_\-\.]+@[a-zA-Z0-9\-\.]+[a-zA-Z]{2,4}/) or (isUpdate and (@customerData[session].email is undefined) )
        console.log "E-Mail Adresse okay!"
      else
        _correctionRequest('Email', session)
        return false

      return true

    _mapApiAiCustomerPayloadToLocalProperties: (apiAiPayload, session) ->
      console.log "Mapping"
      tmp = if @customerData[session]? then @customerData[session] else {}
      for localProperty, apiAiProperty of config.mappings.customerData
        unless apiAiPayload.parameters[apiAiProperty] is undefined
          tmp[localProperty] = apiAiPayload.parameters[apiAiProperty]
      @customerData[session] = tmp

    _correctionRequest = (incorrectItem, session) ->
      botID = if session.match(/\-/) then config.motionAI.botIDWebchat else config.motionAI.botIDFacebook
      startModule = _getStartModule(incorrectItem, session)
      console.log "Send Correction Request"
      options =
        method: 'POST'
        url: 'https://api.motion.ai/messageHuman'
        body:
          bot: botID
          to: session
          key: config.motionAI.apiKey
          startModule: startModule
        json: true
      _sendResponse(options)

    _completeRequest = (session, customerData) ->
      console.log "Complete Request"
      botID = if session.match(/\-/) then config.motionAI.botIDWebchat else config.motionAI.botIDFacebook
      startModule = if session.match(/\-/) then config.motionAI.completionModuleWebchat else config.motionAI.completionModuleFacebook
      nextModule =
        method: 'POST'
        url: 'https://api.motion.ai/messageHuman'
        body:
          bot: botID
          to: session
          key: config.motionAI.apiKey
          startModule: startModule
        json: true
      _sendResponse(nextModule)

    _sendResponse = (options) ->
        request options, (error, response, body) ->
          if error
            throw new Error(error)
          return

    _getStartModule = (incorrectItem, session) ->
      bot = if session.match(/\-/) then "Webchat" else "Facebook"
      if bot is "Webchat"
        switch incorrectItem
          when "Ticketanzahl" then return config.motionAI.webchatStartModuleCorrection.ticketanzahl
          when "Datum" then return config.motionAI.webchatStartModuleCorrection.datum
          when "Vorname" then return config.motionAI.webchatStartModuleCorrection.vorname
          when "Nachname" then return config.motionAI.webchatStartModuleCorrection.nachname
          when "Strasse" then return config.motionAI.webchatStartModuleCorrection.strasse
          when "Stadt" then return config.motionAI.webchatStartModuleCorrection.stadt
          when "Postleitzahl" then return config.motionAI.webchatStartModuleCorrection.postleitzahl
          when "Email" then return config.motionAI.webchatStartModuleCorrection.email
      else if bot is "Facebook"
        switch incorrectItem
          when "Ticketanzahl" then return config.motionAI.facebookStartModuleCorrection.ticketanzahl
          when "Datum" then return config.motionAI.facebookStartModuleCorrection.datum
          when "Vorname" then return config.motionAI.facebookStartModuleCorrection.vorname
          when "Nachname" then return config.motionAI.facebookStartModuleCorrection.nachname
          when "Strasse" then return config.motionAI.facebookStartModuleCorrection.strasse
          when "Stadt" then return config.motionAI.facebookStartModuleCorrection.stadt
          when "Postleitzahl" then return config.motionAI.facebookStartModuleCorrection.postleitzahl
          when "Email" then return config.motionAI.facebookStartModuleCorrection.email

  @get: (apiAiPayload, session) ->
    console.log "Get"
    console.log @_instance
    @_instance ?= new SingletonClass()
