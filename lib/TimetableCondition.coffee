moment = require 'moment'
config = require '../config/config'

module.exports = class TimetableCondition

  constructor: (apiAiPayload, @knex) ->
    @_mapApiAiTimetablePayloadToLocalProperties(apiAiPayload)
    @_createCondition()

  get: ->
    @condition

  _createCondition: () ->
    _this = @
    if @all isnt ""
      @condition = @knex.select().table('festival_data')
    else
      @condition = @knex('festival_data').where(() ->
        if _this.whatsNext is ""
          return @where(_this._processArtistAndStage())
        else
          return @whereRaw(_this._processWhatsNext())
      ).andWhere( () ->
        @whereRaw(_this._processDateAndTime())
      ).andWhere( () ->
        @whereRaw(_this._processDateAndTimePeriod()))

  _mapApiAiTimetablePayloadToLocalProperties: (apiAiPayload) ->
    for localProperty, apiAiProperty of config.mappings.timetable
      @[localProperty] = apiAiPayload.parameters[apiAiProperty]

  _processArtistAndStage: () ->
    conditionObject = {}
    # Nur Artist verfuegbar
    if @artist isnt ''
      conditionObject.artist = @artist
      #Buehne nicht verfügbar
      if @stage is ''
        stageFromArtist = @knex('festival_data').where('artist', @artist).select('stage')
        conditionObject.stage = stageFromArtist

    # Nur Stage verfuegbar
    if @stage isnt ''
      conditionObject.stage = @stage

    return conditionObject

  _processWhatsNext: () ->
    condition = ""
    # Wer spielt als naechstes (nach einem Artist)?
    if @artist isnt '' and @whatsNext isnt ''
      endtime = @knex('festival_data').where('artist', @artist).select('endtime').toString()
      condition = "starttime >= (#{endtime}) AND " +
        "endtime <= ADDDATE((#{endtime}),INTERVAL 5 HOUR)"

      if @stage isnt ''
        condition += " AND stage = ('#{@stage}')"

    return condition

  _processDateAndTime: () ->
    condition = ""
    # Datum oder Tag verfuegbar
    if @date isnt '' or @time isnt ''
      # Nur Tag verfuegbar
      if @date is ''
        @date = moment().format("YYYY-MM-DD")
        dateTime = "#{@date} #{@time}"
        condition = "Starttime <= '#{dateTime}' AND Endtime >= '#{dateTime}'"
        # Nur Datum verfuegbar
      else if @time is ''
        nextDate = moment(@date).add(1, "days").format("YYYY-MM-DD")
        condition = "Starttime <= '#{nextDate}' AND Endtime >= '#{@date}'"
        # Tag und Datum verfuegbar
      else
        dateTime = "#{@date} #{@time}"
        condition = "Starttime <= '#{dateTime}' AND Endtime >= '#{dateTime}'"

    return condition

  _processDateAndTimePeriod: () ->
    condition = ""
    # Time-Period verfuegbar und Datum verfuegbar
    if @timePeriod isnt ''
      #Abfagen, ob Time-Period falsch interpretiert wird
      if @timePeriod is 'mittag' or @timePeriod is 'Mittag'
        @timePeriod = '12:00:00/16:00:00'
      timePeriodContains = @timePeriod.split("/")

      #Pruefen ob Datum verfuegbar
      if @date is ''
        @date = moment().format("YYYY-MM-DD")
      else
        @date = moment(@date).format("YYYY-MM-DD")

      condition  = "(starttime >= '#{@date} #{timePeriodContains[0]}' AND endtime <= '#{@date} #{timePeriodContains[1]}')"
    return condition
