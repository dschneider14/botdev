moment = require('moment')
config = require '../config/config'
spotify = require './spotify/getArtist'
promise = require 'bluebird'

module.exports = class ResultHandling

  constructor: (timetableResults) ->
    @timetableResults = timetableResults

  getResults: () ->
    return await @_prepareAnswer(@timetableResults)

  _prepareAnswer: (timetableResults) ->
    #Query erfolgreich, aber keine Treffer
    if timetableResults.length == 0
      return {responseOverride : "Die Abfrage ergab keinen Treffer"}

    #Resultate als Karte ausgeben
    cardData = []
    for timetableResult in timetableResults
      cardData.push await @_createSingleCardData timetableResult

    return {"cards": cardData}

  _createSingleCardData: (timetableResults) ->
    starttime = moment(timetableResults.starttime).format("D.M H:mm")
    endtime = moment(timetableResults.endtime).format("H:mm")
    spotifyData = await spotify timetableResults.artist
    cardData = {
      cardTitle: "#{timetableResults.artist} 🎵",
      cardSubtitle: "#{timetableResults.stage} / #{starttime} - #{endtime} - Klicke auf das Bild um zu Spotify zu gelangen" ,
    }
    if spotifyData?
      cardData.cardLink = spotifyData.link
      cardData.cardImage = spotifyData.image
    return cardData
