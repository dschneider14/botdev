DebugLogObject = require('debug')

log = {
  verbose: DebugLogObject('verbose')
  debug: DebugLogObject('http')
  error: DebugLogObject('error')

}
log.error.color = DebugLogObject.colors[5] # red

module.exports = log
