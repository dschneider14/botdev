FROM node:8.4.0-alpine

WORKDIR /usr/src/app

COPY package.json .

RUN npm install
RUN npm install -g coffeescript@next
RUN npm install -g coffee-script

COPY . .

EXPOSE 3000

VOLUME ["/usr/src/app/node_modules"]

CMD [ "npm", "start" ]
