exports.up = (knex, Promise) ->
  knex.schema.createTableIfNotExists 'festival_data', (table) ->
    table.increments()
    table.string 'artist'
    table.string 'stage'
    table.dateTime 'starttime'
    table.dateTime 'endtime'
    table.string 'token'

exports.down = (knex, Promise) ->
  knex.schema.dropTable 'festival_data'
