moment = require 'moment'

makeTime = (days, time) ->
  splitted = time.split ":"
  hours = splitted.shift()
  minutes = splitted.shift()
  moment().add(days, 'days').seconds(0).minutes(minutes).hours(hours).milliseconds(0).toDate()

today = (time) ->
  makeTime 0, time

tomorrow = (time) ->
  makeTime 1, time

tdaTomorrow = (time) ->
  makeTime 2, time

threeDaysFromToday = (time) ->
  makeTime 3, time

exports.seed = (knex, Promise) ->
  knex('festival_data').del()
  .then () ->
    knex('festival_data').insert([
      {id: 1, artist: 'Lower Than Atlantis', stage:'Zeppelin Stage', starttime: today('13:10'), endtime: today('13:50'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 2,  artist: 'Donots', stage:  'Zeppelin Stage', starttime: today('14:15'), endtime: today('15:05'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 3,  artist: 'Sum 41',stage: 'Zeppelin Stage', starttime: today('15:30'), endtime: today('16:40'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 4,  artist: 'Wirtz',stage: 'Zeppelin Stage', starttime: today('17:10'), endtime: today('18:20'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 5,  artist: 'Beatsteaks', stage: 'Zeppelin Stage', starttime: today('10:30'), endtime: today('11:30'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 6,  artist: 'Die Toten Hosen',stage: 'Zeppelin Stage', starttime: today('21:00'), endtime: today('23:00'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 7,  artist: 'RIN',stage: 'Becks Park Stage', starttime: today('12:05'), endtime: today('12:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 8, artist: 'Haiyti',stage: 'Becks Park Stage', starttime: today('13:05'), endtime: today('13:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 9, artist: 'Machine Gun Kelly',stage: 'Becks Park Stage', starttime: today('14:10'), endtime: today('15:05'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 10, artist: 'Crystal Fighters',stage: 'Becks Park Stage', starttime: today('15:30'), endtime: today('16:25'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 11, artist: 'Dat Adam',stage: 'Becks Park Stage', starttime: today('16:50'), endtime: today('17:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 12, artist: '187 Strassenbande',stage: 'Becks Park Stage', starttime: today('18:10'), endtime: today('19:15'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 13, artist: 'Bonez MC & Raf Camora',stage: 'Becks Park Stage', starttime: today('19:45'), endtime: today('20:50'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 14, artist: 'Beginner',stage: 'Becks Park Stage', starttime: today('21:25'), endtime: today('22:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 15, artist: 'Kraftklub',stage: 'Becks Park Stage', starttime: today('23:25'), endtime: tomorrow('00:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 16, artist: 'Code Orange', stage:'Zeppelin Stage', starttime: tomorrow('13:50'), endtime: tomorrow('14:30'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 17, artist: 'Gojira',stage: 'Zeppelin Stage', starttime: tomorrow('14:55'), endtime: tomorrow('15:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 18, artist: 'Airbourne', stage:'Zeppelin Stage', starttime: tomorrow('16:10'), endtime: tomorrow('17:10'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 19, artist: 'Alter Bridge',stage: 'Zeppelin Stage', starttime: tomorrow('17:40'), endtime: tomorrow('18:50'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 20, artist: 'Prophets Of Rage',stage: 'Zeppelin Stage', starttime: tomorrow('19:20'), endtime: tomorrow('20:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 21, artist: 'System Of A Down',stage: 'Zeppelin Stage', starttime: tomorrow('21:25'), endtime: tomorrow('23:00'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 22, artist: 'Kaiser Franz Josef',stage: 'Becks Park Stage', starttime: tomorrow('12:35'), endtime: tomorrow('13:10'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 23, artist: 'Frank Carter & The Rattlesnakes',stage: 'Becks Park Stage', starttime: tomorrow('13:30'), endtime: tomorrow('14:15'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 24, artist: 'Slaves',stage: 'Becks Park Stage', starttime: tomorrow('14:40'), endtime: tomorrow('15:25'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 25, artist: 'Henning Wehland',stage: 'Becks Park Stage', starttime: tomorrow('15:50'), endtime: tomorrow('16:40'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 26, artist: 'Feine Sahne Fischfilet',stage: 'Becks Park Stage', starttime: tomorrow('17:05'), endtime: tomorrow('18:05'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 27, artist: 'Jake Bugg',stage: 'Becks Park Stage', starttime: tomorrow('18:30'), endtime: tomorrow('19:30'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 28, artist: 'Genetikk',stage: 'Becks Park Stage', starttime: tomorrow('20:00'), endtime: tomorrow('21:05'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 29, artist: 'Annenmaykantereit',stage: 'Becks Park Stage', starttime: tomorrow('21:35'), endtime: tomorrow('22:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 30, artist: 'Macklemore & Ryan Lewis', stage:'Becks Park Stage', starttime: tomorrow('23:25'), endtime: tdaTomorrow('00:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 31, artist: 'Sondaschule',stage: 'Zeppelin Stage', starttime: tdaTomorrow('13:50'), endtime: tdaTomorrow('14:30'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 32, artist: 'Skindred',stage: 'Zeppelin Stage', starttime: tdaTomorrow('14:55'), endtime: tdaTomorrow('15:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 33, artist: 'In Flames', stage:'Zeppelin Stage', starttime: tdaTomorrow('16:10'), endtime: tdaTomorrow('17:10'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 34, artist: 'Five Finger Death Punch',stage: 'Zeppelin Stage', starttime: tdaTomorrow('17:40'), endtime: tdaTomorrow('18:55'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 35, artist: 'Broilers',stage: 'Zeppelin Stage', starttime: tdaTomorrow('19:25'), endtime: tdaTomorrow('20:50'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 36, artist: 'Rammstein',stage: 'Zeppelin Stage', starttime: tdaTomorrow('21:30'), endtime: tdaTomorrow('23:00'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 37, artist: 'Razz',stage: 'Becks Park Stage', starttime: tdaTomorrow('12:30'), endtime: tdaTomorrow('13:10'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 38, artist: 'Don Broco',stage: 'Becks Park Stage', starttime: tdaTomorrow('13:30'), endtime: tdaTomorrow('14:10'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 39, artist: 'Welshly Arms',stage: 'Becks Park Stage', starttime: tdaTomorrow('14:35'), endtime: tdaTomorrow('15:20'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 40, artist: '2Cellos',stage: 'Becks Park Stage', starttime: tdaTomorrow('15:45'), endtime: tdaTomorrow('16:30'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 41, artist: 'Simple Plan',stage: 'Becks Park Stage', starttime: tdaTomorrow('16:55'), endtime: tdaTomorrow('17:55'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 42, artist: 'Rag \'N\' Bone Man', stage:'Becks Park Stage', starttime: tdaTomorrow('18:20'), endtime: tdaTomorrow('19:25'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 43, artist: 'Liam Gallagher', stage:'Becks Park Stage', starttime: tdaTomorrow('19:55'), endtime: tdaTomorrow('21:00'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 44, artist: 'Bastille', stage:'Becks Park Stage', starttime: tdaTomorrow('21:30'), endtime: tdaTomorrow('22:45'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}
      {id: 45, artist: 'Marteria', stage:'Becks Park Stage', starttime: tdaTomorrow('23:25'), endtime: threeDaysFromToday('00:55'), token: '057e7a1cc6c4416c916168bd7f23f4c4'}


    ])
