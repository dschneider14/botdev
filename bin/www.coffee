###*
# Module dependencies.
###
app = require '../app'
http = require 'http'
normalizePort = require '../lib/utils/normalize-port'

###*
# Get port from environment and store in Express.
###
port = normalizePort(process.env.PORT or '3000')
app.set 'port', port

###*
# Create HTTP server.
###
server = http.createServer(app)

###*
 # Event listener for HTTP server "error" event.
###
server.on 'error', require('../lib/server/on-error').bind(port: port)

###*
 # Event listener for HTTP server "listening" event.
###
server.on 'listening', require('../lib/server/on-listening')

###*
# Listen on provided port, on all network interfaces.
###
server.listen port
