# Start

```
$ npm start
```

# First Setup

- clone repo: git@bitbucket.org:dschneider14/botdev.git
- npm install

### Setup Datebase

1. Create a database and configure .env accordingly
2. Configure knexfile.coffee accordingly
3. Run the commands below to use the local knex to migrate and seed

```
$ nf run node_modules/knex/bin/cli.js migrate:latest
$ nf run node_modules/knex/bin/cli.js seed:run
```

# Development Tasks

### Create Migrations

```
$ node node_modules/knex/bin/cli.js migrate:make -x coffee nameOfTheMigration
```

### Create Seed

```
$ node node_modules/knex/bin/cli.js seed:make -x coffee nameOfTheSeed
```
