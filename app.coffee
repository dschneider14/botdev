express = require('express')
path = require('path')
morganLogger = require('morgan')
cookieParser = require('cookie-parser')
bodyParser = require('body-parser')
fs = require 'fs'
schedule = require 'node-schedule'
child = require 'child_process'

process = require './routes/process'
session = require './routes/session'
customerData = require './routes/customerData'
getToken = require './lib/spotify/getToken'

app = express()

# view engine setup
app.set 'views', path.join(__dirname, 'views')
app.set 'view engine', 'hbs'

morganLoggerStream = fs.createWriteStream(path.join(__dirname, 'logs', 'access.log'), {flags: 'a'})
app.use morganLogger('combined', stream: morganLoggerStream)

app.use bodyParser.json()
app.use bodyParser.urlencoded(extended: false)
app.use cookieParser()
app.use express.static(path.join(__dirname, 'public'))

#Refresh Seed
schedule.scheduleJob('1 0 * * *', ->
  child.exec 'node node_modules/knex/bin/cli.js seed:run'
  return
)

app.use '/process', process
app.use '/session', session
app.use '/customerdata', customerData

#catch 404 and forward to error handler
app.use (req, res, next) ->
  err = new Error('Not Found')
  err.status = 404
  next err
  return

#error handler
app.use (err, req, res, next) ->
  #set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = if req.app.get('env') == 'development' then err else {}
  #render the error page
  res.status err.status or 500
  res.render 'error'
  return

module.exports = app
