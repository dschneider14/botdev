express = require 'express'
QueryService = require '../lib/QueryService'
NavigationService = require '../lib/google/NavigationService'
CustomerDataService = require '../lib/CustomerDataService'
StageNavigationService = require '../lib/StageNavigationService'
Begruessung = '../lib/Begruessung'
log = require '../lib/utils/log'
config = require '../config/config'
prepareApiAiRequest = require '../lib/api-ai/prepare-request'
_ = require 'lodash'

router = express.Router()

customerData = {}

_extractMotionAiQuestion = (req) ->
  return req.body.reply

_handleApiAiResponse = (response) ->
  log.verbose this

  apiAiPayload = response.result
  console.log apiAiPayload

  if apiAiPayload.metadata.intentId is config.intentIds.timetable
    try
      timetableResponse = await new QueryService().executeTimetableQuery @apiAiToken, apiAiPayload
      @res.status(200).json timetableResponse
    catch error
      @res.status(500).send 'Internal Server Error'

  else if _.includes([config.intentIds.navigation, config.intentIds.correction.zipCode, config.intentIds.correction["geo-city"],config.intentIds.correction["street-address"]], apiAiPayload.metadata.intentId) 
    try
      navigationURL = await new NavigationService(apiAiPayload).getNavigation(apiAiPayload)
      @res.status(200).json navigationURL
    catch error
      @res.status(500).send 'Internal Server Error'

  else if apiAiPayload.fulfillment.messages.length > 1
    message = _.find apiAiPayload.fulfillment.messages, {"type": 4}
    @res.status(200).json message.payload

  else if apiAiPayload.metadata.intentId is config.intentIds.stageNavigation
    try
      stageNavigationCard = await new StageNavigationService(apiAiPayload).getCard()
      @res.status(200).json stageNavigationCard
    catch error
      @res.status(500).send 'Internal Server Error'

  else
    console.log
    @res.status(200).json
      responseOverride : apiAiPayload.fulfillment.speech

### POST process listing. ###
router.post '/:api_ai_token', (req, res, next) ->
  apiAiToken = req.params.api_ai_token
  session = if req.body.session.match(/\-/) then req.body.session else req.body.from
  apiAiRequest = prepareApiAiRequest apiAiToken, _extractMotionAiQuestion(req)
  apiAiRequest.on 'response', _handleApiAiResponse.bind({res: res, apiAiToken: apiAiToken, session: session})
  apiAiRequest.on 'error', (error) -> return log.error error
  apiAiRequest.end()

module.exports = router
