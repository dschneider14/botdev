express = require 'express'
request = require 'request'
moment = require 'moment'
schedule = require 'node-schedule'
config = require '../config/config'

router = express.Router()
init = false
loggerObject = {}

### POST process listing. ###
router.post '/', (req, res) ->
  session = if req.body.session.match(/\-/) then req.body.session else req.body.from
  bot = if req.body.session.match(/\-/) then config.motionAI.botIDWebchat else config.motionAI.botIDFacebook

  if session of loggerObject
    loggerObject[session].moduleID = req.body.moduleID
    loggerObject[session].updatedAt = req.body.updatedAt
    loggerObject[session].botID = bot
    loggerObject[session].counter += 1

  else
    loggerObject[session] =
      moduleID: req.body.moduleID
      updatedAt: req.body.updatedAt
      botID: bot
      counter: 0

  if loggerObject[session].counter is 20 #and process.env.ADVERTISING
    loggerObject[session].counter = 0
    _sendAdvertisement session, loggerObject[session].botID


  if not init and process.env.ADVERTISING
    init = true
    schedule.scheduleJob '*/30  * * * * *', () ->
      for currentSession, data of loggerObject
        lastRequest = moment(data.updatedAt, 'YYYYMMDD, h:mm:ss')
        if (moment().toDate() - lastRequest.toDate()) > 30000 #weitere Nachricht alle 30 Sekunden
          _reminder currentSession, data.botID


  console.log '+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+'
  console.log session, '**', loggerObject[session].moduleID, '**' ,loggerObject[session].updatedAt, '**', loggerObject[session].counter
  res.send 'OK'

_sendAdvertisement = (sessionID, bot) ->
  options =
    method: 'POST'
    url: 'https://api.motion.ai/messageHuman'
    body:
      cards: [{
        "cardTitle": "Wie wäre es mit einem Beck's?",
        "cardSubtitle": "Beck's - Partner des Festivals"
        "cardLink": "https://www.becks.de/",
        "cardImage": "https://upload.wikimedia.org/wikipedia/en/thumb/0/08/Becks_Logo.svg/800px-Becks_Logo.svg.png"
        },
        {
        "cardTitle": "Festivalguide",
        "cardSubtitle": "Deine Seite rund um Festivals"
        "cardLink": "http://www.festivalguide.de/",
        "cardImage": "http://www.festivalguide.de/bundles/introwww/img/icons@2x/logo_festivalguide.png"
        }]
      bot: bot
      to: sessionID
      key: config.motionAI.apiKey
      msg: 'Werbung:'
    json: true
  _sendResponse options


_reminder = (sessionID, bot) ->
  options =
    method: 'POST'
    url: 'https://api.motion.ai/messageHuman'
    body:
      bot: bot
      to: sessionID
      key: config.motionAI.apiKey
      msg: 'Hast du noch Fragen zum Festival? Dann schreibe mich doch an ;-)'
    json: true
  _sendResponse options


_sendResponse = (options) ->
  request options, (error, response, body) ->
    if error
      throw new Error(error)
    return

module.exports = router
