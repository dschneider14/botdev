express = require 'express'
QueryService = require '../lib/QueryService'
NavigationService = require '../lib/google/NavigationService'
CustomerDataService = require '../lib/CustomerDataService'
StageNavigationService = require '../lib/StageNavigationService'
Begruessung = '../lib/Begruessung'
log = require '../lib/utils/log'
config = require '../config/config'
prepareApiAiRequest = require '../lib/api-ai/prepare-request'
_ = require 'lodash'

router = express.Router()

customerData = {}

_extractMotionAiQuestion = (req) ->
  return req.body.reply

_handleApiAiResponse = (response) ->
  log.verbose this

  apiAiPayload = response.result
  console.log apiAiPayload


  if apiAiPayload.metadata.intentId is config.intentIds.customerData
    try
      await CustomerDataService.get().addCustomerData(apiAiPayload, @session)
    catch error
      @res.status(500).send "Internal Server Error, #{error}"

  else if _.includes(config.intentIds.correction, apiAiPayload.metadata.intentId)
    try
      await CustomerDataService.get().updateCustomerData(apiAiPayload, @session)
    catch error
      @res.status(500).send "Internal Server Error, #{error}"

  else
    console.log
    @res.status(200).json
      responseOverride : apiAiPayload.fulfillment.speech

### POST process listing. ###
router.post '/:api_ai_token', (req, res, next) ->
  apiAiToken = req.params.api_ai_token
  session = if req.body.session.match(/\-/) then req.body.session else req.body.from
  apiAiRequest = prepareApiAiRequest apiAiToken, _extractMotionAiQuestion(req)
  apiAiRequest.on 'response', _handleApiAiResponse.bind({res: res, apiAiToken: apiAiToken, session: session})
  apiAiRequest.on 'error', (error) -> return log.error error
  apiAiRequest.end()

module.exports = router
