request = require 'request'
readline = require('readline')
config = require '../config/config'

options =
  method: 'GET'
  url: 'https://api.motion.ai/messageBot'
  qs:
    bot: '79649'
    msg: ''
    session: '123456'
    key: config.motionAI.apiKey



rl = readline.createInterface(
  input: process.stdin
  output: process.stdout
)

rl.question 'Was möchtest du wissen?', (answer) =>
  options.qs.msg = answer
  rl.close()

  request options, (error, response, body) ->
    if error
      throw new Error(error)

    console.log body

    exec = require('child_process').exec
    exec "touch test_console.coffee"
