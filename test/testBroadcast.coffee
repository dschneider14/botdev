request = require 'request'
Promise = require 'bluebird'
_ = require 'lodash'
config = require '../config/config'
DatabaseConnector = require('../config/DatabaseConnector')


request 'https://api.motion.ai/1.0/getConversations?key=bc71236cbc7104199839ccea2d5180b8&direction=in'
,(error, response, body) ->
    if error
      console.log 'error:', error
    else
      session = []
      body = JSON.parse body
      for key, value of body.messages
        session[key] = value.session
      session = _.uniq(session)
      counter = 0
      for i in session
        counter++
        _sendBroadcast(i)
      console.log counter

    # @knex = new DatabaseConnector().getConnection()
    # @knex.schema.createTableIfNotExists 'sessions', (table) ->
    #   table.increments()
    #   table.string 'session'
    #   return



_sendBroadcast = (sessionID) ->
  options =
    method: 'POST'
    url: 'https://api.motion.ai/messageHuman'
    body:
      bot: 82676
      to: sessionID
      key: config.motionAI.apiKey
      msg: 'Das sind tolle Neuigkeiten'
    json: true
  request options, (error, response, body) ->
    if error
      throw new Error(error)
    console.log body
    return
