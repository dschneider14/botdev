Condition = require('../public/javascripts/condition')
ConnectionPool = require('../config/ConnectionPool')

data =
    source: 'agent'
    resolvedQuery: 'Wann spielt Rammstein?'
    action: 'Auskunft'
    actionIncomplete: false
    parameters:
      artist: 'Rammstein'
      date: ''
      stage: ''
      time: ''
      'time-period': ''
      WhatsNext: ''


  pool = new ConnectionPool()
  pool.getConnection().then (connection) ->
    console.log connection

    sqlStatement = "SELECT * FROM botDev.FestivalData;"
    connection.query sqlStatement, (error, results, fields) ->
