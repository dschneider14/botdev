request = require 'request'
config = require '../config/config'

options =
  method: 'POST'
  url: 'https://api.motion.ai/messageHuman'
  body:
    to: 'a605d2a1-c56e-41ea-b532-6ed1629ed53c'
    bot: config.motionAI.botIDWebchat
    key: config.motionAI.apiKey
    startModule: 1162740
  json: true

request options, (error, response, body) ->
  if error
    throw new Error(error)
  console.log body
  return
